# from fastapi import FastAPI
# from fastapi.security import OAuth2PasswordBearer
# from passlib.context import CryptContext

# app = FastAPI()
# oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/token")
# pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

# @app.get("/")
# async def root():
#     return {"message": "Hello World"}

# @app.post("/register")
# def register_user(username: str, password: str):
#     hashed_password = pwd_context.hash(password)
#     # Сохраните пользователя в базе данных
#     return {"username": username, "hashed_password": hashed_password}


# @app.post("/token")
# def authenticate_user(username: str, password: str):
#     user = get_user(username)
#     if not user:
#         raise HTTPException(status_code=400, detail="Incorrect username or password")

#     is_password_correct = pwd_context.verify(password, user.hashed_password)

#     if not is_password_correct:
#         raise HTTPException(status_code=400, detail="Incorrect username or password")
#     jwt_token = create_jwt_token({"sub": user.username})
#     return {"access_token": jwt_token, "token_type": "bearer"}

import uvicorn

if __name__ == "__main__":
    uvicorn.run("app.api:app", host="127.0.0.1", port=8081, reload=True)